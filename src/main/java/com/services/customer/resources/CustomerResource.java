package com.services.customer.resources;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.services.customer.model.CustomerModel;


@RestController
public class CustomerResource {
	
	private final AtomicLong counter = new AtomicLong();
	
	//Map to store employees, ideally we should use database
	Map<Long, CustomerModel> customers = new HashMap<Long, CustomerModel>();
   
    @RequestMapping("/getCust")
    public CustomerModel getCustomer(
    		@RequestParam(value="id") long id) {
    	return customers.get(id);
    }
    
    @RequestMapping("/deleteCust")
    public CustomerModel deleteCustomer(
    		@RequestParam(value="id") long id){
    	CustomerModel cust = customers.get(id);
    	customers.remove(id);
        return cust;
    }
    
    @RequestMapping("/addCust")
    public CustomerModel addCustomer(
    		@RequestParam(value="fname") String fname,
    		@RequestParam(value="lname") String lname,
    		@RequestParam(value="addr") String addr,
    		@RequestParam(value="phone") String phone){
    	CustomerModel cust = new CustomerModel(fname, lname, addr, phone);
    	customers.put(counter.incrementAndGet(), cust);	
    	return cust;
    	
    }
    
    @RequestMapping("/updateCust")
    public CustomerModel updateCustomer(
    		@RequestParam(value="id") long id,
    		@RequestParam(value="fname") String fname,
    		@RequestParam(value="lname") String lname,
    		@RequestParam(value="addr") String addr,
    		@RequestParam(value="phone") String phone){
    	
    	CustomerModel cust = customers.get(id);
    	cust.setFirstName(fname);
    	cust.setLastName(lname);
    	cust.setAddress(addr);
    	cust.setPhone(phone);
    	customers.put(id, cust);
    	
    	return cust;
    	
    }
}
