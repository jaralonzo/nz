# README #

Restful Services using Java and Springboot

### What is this repository for? ###

* For Java Full Stack Development – Technical Test - Recruit I.T.

### How do I get set up? ###

* Run mvn install
* Run mvn spring-boot:run
* Access thru:

* GET localhost:8080/getCust?id=<CustomerID>

* UPDATE localhost:8080/updateCust?id=<CustomerID>&fname=<FirstName>&lname=<LastName>&addr=<Address>&phone=<PhoneNumber>

* ADD localhost:8080/addCust?fname=<FirstName>&lname=<LastName>&addr=<Address>&phone=<PhoneNumber>

* DELETE localhost:8080/deleteCust?id<CustomerID>